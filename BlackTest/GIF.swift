//
//  GIF.swift
//  BlackTest
//
//  Created by Nicholas Allio on 03/11/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

struct GIFFields {
    static let images = "images"
    static let original = "original"
    static let url = "url"
}

class GIF: NSObject {
    let urlGIF: String
    var caption: String?
    
    init(url: String) {
        self.urlGIF = url
    }
    
    static func GIFsFromResult(result: [[String:AnyObject]]) -> [GIF] {
        var gifs = [GIF]()
        
        for gif in result {
            let imagesArray = gif[GIFFields.images] as! [String:[String:AnyObject]]
            let original = imagesArray[GIFFields.original]!
            let newElement = GIF(url: original[GIFFields.url] as! String)
            gifs.append(newElement)
        }
        
        return gifs
    }
}
