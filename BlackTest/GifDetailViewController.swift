//
//  GifDetailViewController.swift
//  BlackTest
//
//  Created by Nicholas Allio on 08/11/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import WebKit

class GifDetailViewController: UIViewController {
    
    var gifContainer: WKWebView!
    var progressView: UIProgressView!
    
    var urlGIF: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.progressView = UIProgressView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: 2))
        self.progressView.transform = CGAffineTransform(scaleX: 1, y: 2)
        
        self.gifContainer = WKWebView(frame: self.view.frame)
        self.gifContainer.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        self.gifContainer.isUserInteractionEnabled = false
        self.gifContainer.scrollView.isScrollEnabled = false
        
        self.gifContainer.load(URLRequest(url: URL(string: self.urlGIF)!))
        
        self.view.insertSubview(self.gifContainer, at: 0)
        self.view.addSubview(self.progressView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.gifContainer.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.isHidden = self.gifContainer.estimatedProgress == 1 // if progress = 1, page is loaded then set isHidden true
            self.progressView.progress = Float(self.gifContainer.estimatedProgress)
        }
    }

}
