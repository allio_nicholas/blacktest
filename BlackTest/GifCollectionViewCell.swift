//
//  GifCollectionViewCell.swift
//  BlackTest
//
//  Created by Nicholas Allio on 04/11/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import WebKit

class GifCollectionViewCell: UICollectionViewCell {
    
    var GifContainer: WKWebView!
    var progressView: UIProgressView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.GifContainer = WKWebView(frame: self.frame)
        self.GifContainer.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        self.GifContainer.isUserInteractionEnabled = false
        self.GifContainer.scrollView.isScrollEnabled = false
        
        self.progressView = UIProgressView(frame: CGRect(x: 0, y: contentView.frame.height - 3, width: contentView.frame.width, height: 2))
        self.progressView.transform = CGAffineTransform(scaleX: 1, y: 2)
        
        contentView.addSubview(self.progressView)
        contentView.insertSubview(self.GifContainer, at: 0)
        
    }
    
    deinit {
        self.GifContainer.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
    }
    
    func setGIF(url: String) {
        self.GifContainer.load(URLRequest(url: URL(string: url)!))
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.isHidden = GifContainer.estimatedProgress == 1 // if progress = 1, page is loaded then set isHidden true
            self.progressView.progress = Float(self.GifContainer.estimatedProgress)
        }
    }
}
