//
//  GIFsHandler.swift
//  BlackTest
//
//  Created by Nicholas Allio on 02/11/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

class GIFsHandler: NSObject {
    fileprivate let baseURL = "http://api.giphy.com/v1/gifs/"
    fileprivate let publicGIFYKey = "dc6zaTOxFJmzC"
    fileprivate let session = URLSession.shared
    
    func getTrendingGIFs(_ completion: @escaping (([GIF]?) -> ())) {
        let url = URL(string: baseURL + "trending?api_key=" + publicGIFYKey)
        let request = URLRequest(url: url!)
        
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard (error == nil) else {
                completion(nil)
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                completion(nil)
                return
            }
            
            guard let data = data else {
                completion(nil)
                return
            }
            
            self.convertData(with: data, completionHandler: completion)
            
        })
        
        dataTask.resume()
    }
    
    private func convertData(with data: Data, completionHandler: @escaping (([GIF]?) -> ())) {
        do {
            let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
            let GIFsList = jsonData["data"] as! [[String:AnyObject]]

            completionHandler(GIF.GIFsFromResult(result: GIFsList))
            
        } catch {
            completionHandler(nil)
        }
    }

}
