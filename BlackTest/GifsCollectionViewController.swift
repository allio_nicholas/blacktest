//
//  GifsCollectionViewController.swift
//  BlackTest
//
//  Created by Nicholas Allio on 03/11/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

private let reuseIdentifier = "GIF"

class GifsCollectionViewController: UICollectionViewController {
    
    var gifs = [GIF]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadGIFs()

    }

    func loadGIFs() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let gifHandler = GIFsHandler()
        gifHandler.getTrendingGIFs({ (result) in
            if let result = result {
                self.gifs = result
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            } else {
                let alert = UIAlertController(title: "Error", message: "An error occurred while downloading the GIFs.\nCheck your connection and retry.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                    self.loadGIFs()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let destVC = segue.destination as! GifDetailViewController
            let indexPathSender = self.collectionView?.indexPath(for: sender as! GifCollectionViewCell)
            destVC.urlGIF = self.gifs[indexPathSender!.row].urlGIF
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gifs.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GifCollectionViewCell
    
        let current = self.gifs[indexPath.row]
        // Configure the cell
        cell.setGIF(url: current.urlGIF)
    
        return cell
    }

}
